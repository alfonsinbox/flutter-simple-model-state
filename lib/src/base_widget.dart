import 'package:flutter/widgets.dart';
import 'package:provider/provider.dart';

import 'base_model.dart';

class BaseWidget<T extends BaseModel> extends StatefulWidget {
  final T model;
  final Widget Function(BuildContext context, T value, Widget? child) builder;
  final Widget? child;

  const BaseWidget({
    Key? key,
    required this.model,
    required this.builder,
    this.child,
  }) : super(key: key);

  @override
  _BaseWidgetState<T> createState() => _BaseWidgetState<T>();
}

class _BaseWidgetState<T extends BaseModel> extends State<BaseWidget<T>> {
  @override
  Widget build(BuildContext context) {
    return ChangeNotifierProvider<T>(
      create: (context) => widget.model,
      child: Consumer<T>(
        child: widget.child,
        builder: widget.builder,
      ),
    );
  }
}

import 'package:flutter/foundation.dart';

abstract class BaseModel with ChangeNotifier {
  bool _isDisposed = false;
  bool get isDisposed => isDisposed;

  @override
  void dispose() {
    _isDisposed = true;
    super.dispose();
  }

  /// Notify listeners, but only if model has not been disposed.
  @override
  void notifyListeners() {
    if (_isDisposed) return;
    super.notifyListeners();
  }
}
